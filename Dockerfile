FROM alpine:3.10

ENV ANSIBLE_VERSION 2.9.2

RUN echo "===> Installing sudo ..."  && \
    apk --update add sudo            && \
    \
    \
    echo "===> Adding Python runtime. .."  && \
    apk update && apk add --no-cache python3 && \
    apk --update add py-pip openssl ca-certificates    && \
    apk --update add --virtual build-dependencies python-dev \
        libffi-dev openssl-dev build-base  && \
    \
    \
    echo "===> Installing tools: yamllint, netaddr, jmespath, ansible ..."  && \
    pip install --upgrade pip cffi yamllint netaddr jmespath  \
        ansible==${ANSIBLE_VERSION} && \
    \
    \
    echo "===> Installing handy tools (not absolutely required) ..."  && \
    pip install --upgrade pip pycrypto pywinrm         && \
    apk --update add sshpass openssh-client rsync tree  && \
    \
    \
    echo "===> Removing package list..."  && \
    apk del build-dependencies            && \
    rm -rf /var/cache/apk/*               && \
    \
    \
    echo "===> Adding hosts for convenience..."  && \
    mkdir -p /etc/ansible                        && \
    echo 'localhost' > /etc/ansible/hosts

# Ansible Configuraiton
#   https://docs.ansible.com/ansible/latest/reference_appendices/config.html
ENV ANSIBLE_CONFIG /ansible/playbooks/ansible.cfg
ENV ANSIBLE_GATHERING smart
ENV ANSIBLE_HOST_KEY_CHECKING false
ENV ANSIBLE_RETRY_FILES_ENABLED false
ENV ANSIBLE_ROLES_PATH /ansible/playbooks/roles
ENV ANSIBLE_SSH_PIPELINING True
ENV PYTHONPATH /ansible/lib
ENV PATH /ansible/bin:$PATH
ENV ANSIBLE_LIBRARY /ansible/library

WORKDIR /ansible/playbooks

# default command: display Ansible version
CMD [ "ansible-playbook", "--version" ]
